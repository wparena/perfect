
<?php get_header(); ?>
<div class="clearfix"></div>
<!-- =========================
     Page Content Section      
============================== -->
 <main id="content">

  <?php get_template_part('navbar','');?>

    <div class="main-layout">
      <div class="row">
      <div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-md-12 col-lg-12"; } else { echo "col-md-9 col-lg-9"; } ?>">
        <div class="post-area">
		      <?php if(have_posts())
		        {
		      while(have_posts()) { the_post(); ?>
            <div class="pft-blog-post-box">
              <?php if(has_post_thumbnail()): ?>
               <?php $defalt_arg =array('class' => "img-responsive"); ?>
               <?php the_post_thumbnail('', $defalt_arg); ?>
              <?php endif; ?>
              <article class="small">

                <h1><a><?php the_title(); ?></a></h1>

                <div class="pft-blog-category post-meta-data"> 
          
                  <i class="fa fa-user"></i><?php the_author_posts_link(); ?>
                  <i class="fa fa-calendar"></i><span><?php echo get_the_date( get_option( 'date_format' ) ); ?></span>
                  
                </div>

                <?php the_content(); ?>
                <?php wp_link_pages( array( 'before' => '<div class="link single-page-break">' . __( 'Pages:', 'perfect' ), 'after' => '</div>' ) ); ?>
                <div class="category-tag-div">
                  <?php the_tags( '<i class="fa fa-tag" aria-hidden="true"></i> ', ', ', '<br />' ); ?>
                </div>
              </article>
              <div class="social-content">
                <h2>Enjoy the post?</h2>
                <p>Consider sharing it on the following social bookmarking sites.</p>
                <ul class="pft-social share-post">
                  <a href="<?php echo 'http://www.facebook.com/share.php?u=';the_permalink(); ?>"> <li><span class="icon-soci"> <i class="fa fa-facebook"></i></span></li> </a>
                  
                  <a href="<?php echo 'https://www.linkedin.com/shareArticle?url=';the_permalink(); ?>" > <li><span class="icon-soci"><i class="fa fa-linkedin"></i></span></li> </a>
                  
                  <a href="<?php echo 'http://twitter.com/home?status=';the_permalink(); ?>" > <li><span class="icon-soci"><i class="fa fa-twitter"></i></span></li> </a>
                  
                  <a href="<?php echo 'https://plus.google.com/share?url=';the_permalink(); ?>" > <li><span class="icon-soci"><i class="fa fa-google-plus"></i></a></span></li> </a>
                </ul>
              </div>
            </div>
		      <?php } ?>
		      <?php } ?>
         <?php comments_template('',true); ?>
        </div>
      </div>
      <aside class="col-md-3 col-lg-3">
      <?php get_sidebar(); ?>
      </aside>
      </div>
    </div>
    <!--/ Row end -->
</main>
<?php get_footer(); ?>