<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @package Perfect
 */
get_header(); ?>
<main id="content">

	<?php get_template_part('navbar','');?>
    <div class="main-layout">
    	<div class="row">
			<!-- Blog Area -->
			<div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-md-12 col-lg-12"; } else { echo "col-md-9 col-lg-9"; } ?>">
				<div class="post-area">
					<?php if( have_posts()) :  the_post(); ?>
						<h1><?php echo esc_html(get_the_title()); ?></h1>	
						<?php the_content(); ?>
						<?php endif; ?>
						<?php wp_link_pages( array( 'before' => '<div class="link page-break-links">' . __( 'Pages:', 'perfect' ), 'after' => '</div>' ) ); ?>
						<?php comments_template( '', true ); // show comments ?>
					<!-- /Blog Area -->			
				</div>
			</div>
			<!--Sidebar Area-->
			<aside class="col-md-3 col-lg-3">
				<?php get_sidebar(); ?>
			</aside>
			<!--Sidebar Area-->
		</div>
	</div>
</main>
<?php
get_footer();