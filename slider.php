<?php if(is_home()): ?>
    <div class="carouselcontainer">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <?php $i = 0; ?>
          <?php if(have_posts()) {
                  while(have_posts()){
                          the_post();
          ?>

          <?php if($i==0) { ?>
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <?php } else { ?>
          <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>"></li>
          
          <?php } ?>

          <?php $i++; } } ?>
        </ol>

        <?php $i=1; ?>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          
          <?php if(have_posts()) {
                  while(have_posts()){
                          the_post();
          ?>
          <?php if($i==1) { ?>
            <div class="item active">
              <?php if( has_post_thumbnail() ) { ?>
                <a href="<?php the_permalink();?>"><img src="<?php echo esc_url(wp_get_attachment_url(get_post_thumbnail_id())); ?>" alt="<?php the_title_attribute();?>"></a>
              <?php } else { ?>
                <a href="<?php the_permalink();?>"><img src="<?php echo get_template_directory_uri().'/images/perfect.jpg' ?>" alt="<?php the_title_attribute();?>"></a>
              <?php } ?>
                <a href="<?php the_permalink();?>">
                  <div class="carousel-caption">
                    <h3><?php the_title();?></h3>
                  </div>
                </a>
            </div>
          <?php } else { ?>
            <div class="item">
              <?php if( has_post_thumbnail() ) { ?>
                <a href="<?php the_permalink();?>"><img src="<?php echo esc_url(wp_get_attachment_url(get_post_thumbnail_id())); ?>" alt="<?php the_title_attribute();?>"></a>
              <?php } else { ?>
                <a href="<?php the_permalink();?>"><img src="<?php echo get_template_directory_uri().'/images/perfect.jpg' ?>" alt="<?php the_title_attribute();?>"></a>
              <?php } ?>
              <a href="<?php the_permalink();?>">
                <div class="carousel-caption">
                  <h3><?php the_title();?></h3>
                </div>
              </a>
            </div>
          <?php } ?>

          <?php $i++; }} ?>

        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
<?php endif; ?>
