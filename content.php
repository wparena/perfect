<?php 
/**
 * The template for displaying the content.
 * @package Perfect
 */
?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="pft-blog-post-box">
			<article class="small">
				<h1><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">
				  <?php the_title(); ?>
				  </a>
				</h1>
				<div class="pft-blog-category post-meta-data"> 
					
					<i class="fa fa-user"></i><?php the_author_posts_link(); ?>
					<i class="fa fa-calendar"></i><span><?php echo get_the_date( get_option( 'date_format' ) ); ?></span>
					
				</div>

				<p>
					<?php
						the_excerpt();
					?>

					<?php wp_link_pages( array( 'before' => '<div class="link">' . __( 'Pages:', 'perfect' ), 'after' => '</div>' ) ); ?>

				<?php
					if (('post' === get_post_type() && !post_password_required()) && (comments_open() || get_comments_number())) :
						if ((has_post_thumbnail()) || (!has_post_thumbnail() && !is_single())) :
							?>
							<div class="comments-count">
								<a href="<?php esc_url(comments_link()); ?>">
									<?php
									echo "Comments ".number_format_i18n(get_comments_number());
									?>
								</a>
							</div>
							<?php
						endif;
					endif;
				?>

				<a class="readmore" href="<?php the_permalink();?>">Continue Reading</a>
				</p>
				<div class="categorycontent">
					<i class="fa fa-folder"></i><a href="#">
					<?php   $cat_list = get_the_category_list();
					if(!empty($cat_list)) { ?>
					<?php the_category(', '); ?>
					</a>
					<?php } ?>
				</div>
			</article>
		</div>
	</div>
