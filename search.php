<?php
/**
 * The template for displaying search results pages.
 *
 * @package Perfect
 */

get_header(); 
?>
<div class="clearfix"></div>
<main id="content">
		<?php get_template_part('navbar','');?>
	<div class="main-layout">
	    <div class="row">
	      	<div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-md-12 col-lg-12"; } else { echo "col-md-9 col-lg-9"; } ?>">
	      		<div class="post-area">
			        <?php 
						global $i;
						if ( have_posts() ) : ?>
						<h2><?php printf( esc_html__( "Search Results for: %s", 'perfect' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
						<br>
						<?php while ( have_posts() ) : the_post();  
						 get_template_part('content','');
						 endwhile; else : ?>
						<h2><?php esc_html_e('Not Found','perfect'); ?></h2>
						<div class="">
						<p><?php esc_html_e('Sorry, Do Not match.','perfect' ); ?>
						</p>
						<?php get_search_form(); ?>
						</div><!-- .blog_con_mn -->
					<?php endif; ?>
				</div>
			</div>
			<aside class="col-md-3 col-lg-3">
		      <?php get_sidebar(); ?>
		    </aside>
	   	</div>
	</div>
</main>
<?php
get_footer();
?>