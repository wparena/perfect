<?php
/**
 * The template for displaying archive pages.
 *
 * @package Perfect
 */

get_header(); ?>

<main id="content">
	<?php get_template_part('navbar','');?>
    <div class="main-layout">
    	<div class="row">
      		<div class="<?php if( !is_active_sidebar('sidebar-1')) { echo "col-md-12 col-lg-12"; } else { echo "col-md-9 col-lg-9"; } ?>">
	      		<div class="post-area">
	      			<h1 class="archive_title">
		      			<?php echo the_archive_title(); ?>	
		      		</h1>
					<?php 
						if( have_posts() ) :
						while( have_posts() ): the_post();
						get_template_part('content',''); 
						endwhile; endif;
					?>
		          	<div class="text-center">
			        	<?php
							//Previous / next page navigation
							the_posts_pagination( array(
							'prev_text'          => '<i class="fa fa-long-arrow-left"></i>',
							'next_text'          => '<i class="fa fa-long-arrow-right"></i>',
							'screen_reader_text' => ' ',
							) );
						?>
		          	</div>
	      		</div>
     		</div>
	  		<aside class="col-md-3 col-lg-3">
        		<?php get_sidebar(); ?>
      		</aside>
  		</div>
    </div>
</main>
<?php get_footer(); ?>